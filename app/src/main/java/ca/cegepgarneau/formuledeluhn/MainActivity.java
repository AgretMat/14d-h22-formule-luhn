package ca.cegepgarneau.formuledeluhn;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText etId;
    private Button btSubmit;
    private TextView tvResult;
    private ImageView ivCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etId = findViewById(R.id.et_id);
        btSubmit = findViewById(R.id.bt_submit);
        tvResult = findViewById(R.id.tv_result);
        ivCheck = findViewById(R.id.iv_check);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = etId.getText().toString();
                boolean result = luhnTest(number);
                tvResult.setText(String.valueOf(result));

                Toast.makeText(MainActivity.this, "reponse" + result, Toast.LENGTH_LONG).show();

                if (result) {
                    ivCheck.setImageResource(R.drawable.ok);
                } else {
                    ivCheck.setImageResource(R.drawable.delete);
                }
            }
        });
    }


    public static boolean luhnTest(String number) {
        int s1 = 0, s2 = 0;
        String reverse = new StringBuffer(number).reverse().toString();
        for (int i = 0; i < reverse.length(); i++) {
            int digit = Character.digit(reverse.charAt(i), 10);
            if (i % 2 == 0) {//this is for odd digits, they are 1-indexed in the algorithm
                s1 += digit;
            } else {//add 2 * digit for 0-4, add 2 * digit - 9 for 5-9
                s2 += 2 * digit;
                if (digit >= 5) {
                    s2 -= 9;
                }
            }
        }
        return (s1 + s2) % 10 == 0;
    }

}
